# VSCode DevContainer with PlantUML

This project provides an example of run a PlantUML server in a Docker container to render PlantUML in a Visual Studio Code Dev Container.

## Features

- PlantUML server running in a Docker container
- [PlantUML VS Code extension](https://marketplace.visualstudio.com/items?itemName=jebbs.plantuml) (jebbs.plantuml)

## Requires

* Docker
* VS Code
* VS Code extension "Remote - Containers"

## Architecture

The PlantUML diagram below serves as an example and an overview of how this example works.

```plantuml
@startuml

rectangle Host {
    [VSCode]
}

rectangle DevContainer {
    rectangle Docker-in-Docker {
        [PlantUMLServer]
    }
}

VSCode -> PlantUMLServer : 8080
@enduml
```

## Using

1. Highlight the contents of the PlantUML code block (from `@startuml` through `@enduml` inclusive)
2. Go to `Command Palette` (Crtl+Shift+P)
3. Choose `PlantUML: Preview Current Diagram`
4. Contents of the PlantUML code block can be changed and the preview will update

## How it was built

Create a Docker-in-Docker devcontainer and add the following configuration to devcontainer.json (do not delete or replace existing configuration, only add).

```json
{
	"settings": {
		"plantuml.render": "PlantUMLServer",
		"plantuml.server": "http://localhost:8080/"
	},
	"extensions": [
		"jebbs.plantuml"
	],
	"forwardPorts": [
		8080
	],
	"postStartCommand": "docker run -d -p 8080:8080 plantuml/plantuml-server:jetty"
}
```

---
Copyright &copy; 2022 The HFOSSedu Authors. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/.
